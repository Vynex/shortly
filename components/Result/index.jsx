import { useState } from 'react';
import {
	HistoryButton,
	HistoryEntry,
	HistoryInput,
	HistoryOutput,
	HistorySection,
} from '../../styles/sections/statistics.js';
import theme from '../../styles/theme.js';

const Result = ({ entry }) => {
	const [isCopyActive, setIsCopyActive] = useState(false);

	const handleCopy = () => {
		navigator.clipboard.writeText(entry.short);

		setIsCopyActive(true);

		setInterval(() => {
			setIsCopyActive(false);
		}, 10000);
	};

	return (
		<HistoryEntry bg={theme.white}>
			<HistorySection>
				<HistoryInput color={theme.veryDarkBlue} href={entry.long}>
					{entry.long}
				</HistoryInput>
			</HistorySection>

			<HistorySection>
				<HistoryOutput color={theme.cyan} href={entry.short}>
					{entry.short}
				</HistoryOutput>
				<HistoryButton
					variant={!isCopyActive ? 'primary' : 'secondary'}
					onClick={() => handleCopy(entry)}
				>
					{isCopyActive ? 'Copied!' : 'Copy'}
				</HistoryButton>
			</HistorySection>
		</HistoryEntry>
	);
};

export default Result;
