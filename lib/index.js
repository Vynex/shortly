import axios from 'axios';

const url = 'https://api.shrtco.de/v2/shorten';

export const getShortLink = async (link) => {
	const { data } = await axios.get(url, {
		params: {
			url: link,
		},
	});

	return data;
};
