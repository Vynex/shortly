/** @type {import('next').NextConfig} */

module.exports = {
	reactStrictMode: true,
	compiler: {
		styledComponents: true,
	},

	webpack(config) {
		config.module.rules.push({
			test: /\.svg$/i,
			issuer: { and: [/\.(js|ts|md)x?$/] },
			use: ['@svgr/webpack'],
		});
		
		return config;
	},
};
