import { useState } from 'react';
import Head from 'next/head';
import Image from 'next/image';
import { ThemeProvider } from 'styled-components';

import Result from '../components/Result';
import { getShortLink } from '../lib';

import Logo from '../public/assets/logo.svg';
import IconFacebook from '../public/assets/icon-facebook.svg';
import IconTwitter from '../public/assets/icon-twitter.svg';
import IconPinterest from '../public/assets/icon-pinterest.svg';
import IconInstagram from '../public/assets/icon-instagram.svg';

import {
	Navbar,
	NavbarBranding,
	NavbarLink,
	NavbarLinks,
	NavbarMenu,
} from '../styles/sections/navbar.js';
import { Main, Heading, MainImage } from '../styles/sections/main.js';
import {
	Statistics,
	ShortenContainer,
	ShortenForm,
	ShortenInput,
	ShortenButton,
	HistoryContainer,
	StatisticsBanner,
	StatisticsHeading,
	StatisticsDescription,
} from '../styles/sections/statistics.js';
import {
	FeaturesContainer,
	Feature,
	FeatureLine,
	FeatureBadge,
	FeatureHeading,
	FeatureDescription,
} from '../styles/sections/feature.js';
import { Advert, AdvertHeading, AdvertButton } from '../styles/sections/advert.js';
import {
	Footer,
	FooterBranding,
	FooterLogo,
	FooterSocials,
	FooterLinks,
	FooterColumn,
	FooterHeading,
	FooterLink,
	FooterSmallSocials,
} from '../styles/sections/footer.js';
import theme from '../styles/theme';

export default function Home() {
	const [menuOpen, setMenuOpen] = useState(false);

	const [link, setLink] = useState('');
	const [history, setHistory] = useState([]);

	const handleSubmit = async (e) => {
		e.preventDefault();

		const response = await getShortLink(link);
		setHistory((history) => [
			...history,
			{ long: link, short: response.result.full_short_link },
		]);
	};

	return (
		<ThemeProvider theme={theme}>
			<Head>
				<title>Shortly</title>
				<meta name="description" content="More than just Shorter Links" />
				<link rel="icon" href="/favicon.ico" />
			</Head>

			<Navbar>
				<NavbarBranding>
					<Logo viewBox="0 0 121 33" fill={theme.veryDarkBlue} />
				</NavbarBranding>

				<NavbarLinks menuOpen={menuOpen}>
					<NavbarLink>Features</NavbarLink>
					<NavbarLink>Pricing</NavbarLink>
					<NavbarLink>Resources</NavbarLink>
				</NavbarLinks>

				<NavbarMenu
					menuOpen={menuOpen}
					onClick={() => setMenuOpen((menuOpen) => !menuOpen)}
				>
					<span id="line1" />
					<span id="line2" />
					<span id="line3" />
				</NavbarMenu>
			</Navbar>

			<Main>
				<Heading color={theme.veryDarkBlue}>More than just Shorter Links</Heading>

				<MainImage>
					<Image alt="people" src="/assets/people working.svg" height={550} width={550} />
				</MainImage>
			</Main>

			<Statistics bg={theme.lightGray}>
				<ShortenContainer>
					<ShortenForm onSubmit={handleSubmit}>
						<ShortenInput
							placeholder="Shorten a link here..."
							value={link}
							onChange={({ target }) => setLink(target.value)}
							required
							color="red"
						/>

						<ShortenButton color={theme.white} variant="primary">
							Shorten it!
						</ShortenButton>
					</ShortenForm>
				</ShortenContainer>

				<HistoryContainer>
					{history.map((entry, id) => (
						<Result entry={entry} key={id} />
					))}
				</HistoryContainer>

				<StatisticsBanner>
					<StatisticsHeading color={theme.veryDarkBlue}>
						Advanced Statistics
					</StatisticsHeading>
					<StatisticsDescription color={theme.grayishViolet}>
						Track how your links are performing across the web with our advanced
						statistics dashboard.
					</StatisticsDescription>
				</StatisticsBanner>

				<FeaturesContainer>
					<FeatureLine bg={theme.cyan} />

					<Feature bg={theme.white}>
						<FeatureBadge bg={theme.darkViolet} />

						<FeatureHeading color={theme.veryDarkBlue}>Brand Recognition</FeatureHeading>
						<FeatureDescription color={theme.grayishViolet}>
							Boost your brand recognition with each click. Generic links don{"'"}t mean
							a thing. Branded links help instil confidence in your content.
						</FeatureDescription>
					</Feature>

					<Feature bg={theme.white}>
						<FeatureBadge bg={theme.darkViolet} />

						<FeatureHeading color={theme.veryDarkBlue}>Brand Recognition</FeatureHeading>
						<FeatureDescription color={theme.grayishViolet}>
							Boost your brand recognition with each click. Generic links don{"'"}t mean
							a thing. Branded links help instil confidence in your content.
						</FeatureDescription>
					</Feature>

					<Feature bg={theme.white}>
						<FeatureBadge bg={theme.darkViolet} />

						<FeatureHeading color={theme.veryDarkBlue}>Brand Recognition</FeatureHeading>
						<FeatureDescription color={theme.grayishViolet}>
							Boost your brand recognition with each click. Generic links don{"'"}t mean
							a thing. Branded links help instil confidence in your content.
						</FeatureDescription>
					</Feature>
				</FeaturesContainer>
			</Statistics>

			<Advert>
				<AdvertHeading color={theme.white}>Boost your links today</AdvertHeading>
				<AdvertButton variant="primary" color={theme.white}>
					Get Started
				</AdvertButton>
			</Advert>

			<Footer bg={theme.veryDarkViolet}>
				<FooterBranding>
					<FooterLogo>
						<Logo viewBox="0 0 121 33" height="100%" width="100%" fill="white" />
					</FooterLogo>
					<FooterSocials>
						<div>
							<IconFacebook viewBox="0 0 24 24" width="32" height="32" />
						</div>
						<div>
							<IconTwitter viewBox="0 0 24 24" width="32" height="32" />
						</div>
						<div>
							<IconPinterest viewBox="0 0 24 24" width="32" height="32" />
						</div>
						<div>
							<IconInstagram viewBox="0 0 24 24" width="32" height="32" />
						</div>
					</FooterSocials>
				</FooterBranding>

				<FooterLinks>
					<FooterColumn>
						<FooterHeading color={theme.white}>Features</FooterHeading>

						<FooterLink color={theme.gray}>Link Shortening</FooterLink>
						<FooterLink color={theme.gray}>Branded Links</FooterLink>
						<FooterLink color={theme.gray}>Analytics</FooterLink>
					</FooterColumn>

					<FooterColumn>
						<FooterHeading color={theme.white}>Resources</FooterHeading>

						<FooterLink color={theme.gray}>Blog</FooterLink>
						<FooterLink color={theme.gray}>Developers</FooterLink>
						<FooterLink color={theme.gray}>Support</FooterLink>
					</FooterColumn>

					<FooterColumn>
						<FooterHeading color={theme.white}>Company</FooterHeading>

						<FooterLink color={theme.gray}>About</FooterLink>
						<FooterLink color={theme.gray}>Our Team</FooterLink>
						<FooterLink color={theme.gray}>Careers</FooterLink>
						<FooterLink color={theme.gray}>Contacts</FooterLink>
					</FooterColumn>
				</FooterLinks>

				<FooterSmallSocials>
					<IconFacebook viewBox="0 0 24 24" width="24" height="24" />
					<IconTwitter viewBox="0 0 24 24" width="24" height="24" />
					<IconPinterest viewBox="0 0 24 24" width="24" height="24" />
					<IconInstagram viewBox="0 0 24 24" width="24" height="24" />
				</FooterSmallSocials>
			</Footer>
		</ThemeProvider>
	);
}
