import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
   html, body {
      height: 100%;
      width: 100%;
      padding: 0;
      margin: 0;
    
      font-family: 'Poppins', sans-serif;
      font-size: 18px;

      @media (max-width: 1900px) {
         font-size: 14px;
      }

      @media (max-width: 768px) {
         font-size: 12px;
      }
   }

   * {
      box-sizing: border-box;
   }

   #__next {
      height: 100%
    }

    a {
       text-decoration: none;
    }
`;

export default GlobalStyle;
