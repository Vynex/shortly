import styled from 'styled-components';
import { color, buttonStyle } from 'styled-system';

export const Advert = styled.section`
	padding: 2rem 0;
	min-height: 19.5rem;

	background: url('/assets/Meteor.svg');
	background-size: 100% 100%;
	background-repeat: no-repeat;

	display: flex;
	flex-direction: column;
	justify-content: space-evenly;
	align-items: center;

	@media (max-width: 768px) {
		padding: 7rem 0;
	}
`;

export const AdvertHeading = styled.div`
	${color}

	font-size: 2.8rem;
	font-weight: 700;

	@media (max-width: 768px) {
		font-size: 2.2rem;
		padding: 1rem 0;
		text-transform: uppercase;
	}
`;

export const AdvertButton = styled.button`
	${color}
	${buttonStyle}

	height: 4rem;
	width: 15rem;

	border: none;
	border-radius: 5rem;

	font-weight: 700;
	font-size: 1.4rem;
	font-family: 'Poppins';

	cursor: pointer;
`;
