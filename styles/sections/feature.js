import styled from 'styled-components';
import { color } from 'styled-system';

export const FeaturesContainer = styled.div`
	padding: 10rem 0;
	width: 100%;

	display: flex;
	justify-content: space-around;
	align-items: center;

	@media (max-width: 768px) {
		padding: 2rem 0;

		flex-direction: column;
		align-items: center;
	}
`;

export const Feature = styled.article`
	${color}

	min-height: 21rem;
	width: 28%;
	padding: 2rem 3rem;

	border-radius: 0.5rem;
	position: static;
	z-index: 5;

	display: flex;
	flex-direction: column;

	@media (max-width: 768px) {
		width: 100%;
		margin: 4rem 0;
		align-items: center;
	}
`;

export const FeatureLine = styled.div`
	${color}

	height: 1rem;
	width: 50rem;

	content: '';

	position: relative;
	display: inline-block;
	left: 50rem;
	margin-left: -30rem;
	z-index: 1;

	margin-right: -25rem;

	@media (max-width: 768px) {
		top: 70rem;
		left: 2.5rem;

		margin-top: -50rem;

		height: 50rem;
		width: 1rem;
	}
`;

export const FeatureBadge = styled.div`
	margin-bottom: -4.5rem;
	height: 7rem;
	width: 7rem;
	border-radius: 50%;

	background: url('/assets/icon-brand-recognition.svg');
	background-repeat: no-repeat;
	background-position: 50%;
	background-size: 45%;

	position: relative;
	bottom: 5rem;

	${color}
`;

export const FeatureHeading = styled.h3`
	${color}

	margin: 0;
	padding: 1.5rem 0;

	font-size: 1.7rem;
`;

export const FeatureDescription = styled.div`
	${color}
	font-size: 1.2rem;
`;
