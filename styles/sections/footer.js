import styled from 'styled-components';
import { color } from 'styled-system';

export const Footer = styled.footer`
	${color}

	padding: 4.8rem 0;
	min-height: 22.5rem;

	display: flex;
	justify-content: center;

	@media (max-width: 768px) {
		flex-direction: column;
		align-items: center;
	}
`;

export const FooterBranding = styled.div`
	display: flex;
	flex-direction: column;

	@media (max-width: 768px) {
		padding-bottom: 3rem;
		align-items: center;
	}
`;

export const FooterLogo = styled.div`
	width: 9rem;
`;

export const FooterSocials = styled.div`
	padding: 2.4rem 0;
	fill: #fff;

	flex: 1;
	display: flex;

	& > div {
		margin-right: 2rem;
		cursor: pointer;

		&:hover {
			fill: #2acfcf;
		}
	}

	@media (max-width: 768px) {
		display: none;
	}
`;

export const FooterLinks = styled.div`
	padding: 0 10rem;

	display: flex;

	@media (max-width: 768px) {
		padding: 0;

		flex-direction: column;
		align-items: center;
	}
`;

export const FooterColumn = styled.div`
	padding: 0 3rem;

	@media (max-width: 768px) {
		margin: 1rem 0;
		padding: 0;
		width: 100%;

		text-align: center;
	}
`;

export const FooterHeading = styled.h4`
	${color}

	margin: 0;
	padding-bottom: 1rem;

	font-size: 1.2rem;
`;

export const FooterLink = styled.a`
	${color}

	padding: 0.5rem 0;
	cursor: pointer;
	display: block;

	&:hover {
		color: #2acfcf;
	}
`;

export const FooterSmallSocials = styled.div`
	width: 100%;
	padding: 2.5rem 0;

	fill: #fff;

	flex: 1;
	display: none;
	justify-content: center;

	& > svg {
		margin: 0 1rem;
		cursor: pointer;

		&:hover {
			fill: #2acfcf;
		}
	}

	@media (max-width: 768px) {
		display: flex;
	}
`;
