import styled from 'styled-components';
import { color } from 'styled-system';

export const Main = styled.main`
	min-height: 48rem;
	width: 100%;
	text-align: center;

	display: flex;
	flex-direction: column;
	justify-content: space-between;

	@media (max-width: 768px) {
		height: 70rem;
	}
`;

export const Heading = styled.h1`
	${color}

	margin: 0;
	width: 100%;

	font-size: 7.5rem;
	font-weight: 700;
	text-transform: uppercase;

	line-height: 8.5rem;
	letter-spacing: 0.5rem;

	word-wrap: break-word;

	@media (max-width: 768px) {
		margin: 2rem 0;
		padding: 0 0.5rem;

		line-height: 6rem;
		font-size: 4.5rem;
		letter-spacing: 0;
	}
`;

export const MainImage = styled.div`
	height: 100%;
	width: 100%;

	user-select: none;

	@media (max-width: 768px) {
		padding: 4rem 0;
	}
`;
