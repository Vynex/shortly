import styled from 'styled-components';

export const Navbar = styled.nav`
	padding: 4rem 10rem;
	display: flex;

	@media (max-width: 768px) {
		padding: 3rem 2.5rem;
		justify-content: space-between;
	}
`;

export const NavbarBranding = styled.div`
	margin: 0 2.5rem;

	& > svg {
		height: 45px;
		width: 161px;
	}

	@media (max-width: 400px) {
		margin: 0;

		& > svg {
			height: 33px;
			width: 121px;
		}
	}
`;

export const NavbarLinks = styled.div`
	flex: 1;
	display: flex;

	@media (max-width: 768px) {
		display: flex;

		height: 100vh;
		width: 80vw;
		top: 0;
		right: ${({ menuOpen }) => (menuOpen ? '0' : '-100%')};

		background-color: #fff;
		position: fixed;
		flex-direction: column;
		align-items: center;
		justify-content: center;

		z-index: 10;
		transition: right 500ms ease-in-out;
		box-shadow: 0px 5px 10px 0px #aaa;

		& > a {
			padding: 3rem 0;
			font-size: 2rem;
		}
	}
`;

export const NavbarLink = styled.a`
	padding: 0.2rem 1.1rem;

	color: #9e9aa7;
	font-weight: 700;
	font-size: 1.1rem;

	cursor: pointer;

	&:hover {
		color: #35323e;
	}
`;

export const NavbarMenu = styled.div`
	margin: 0.3rem 0;
	display: none;
	flex-direction: column;
	align-items: center;

	position: absolute;
	right: 2.5rem;

	cursor: pointer;

	z-index: 15;

	& > span {
		margin: 2px 0;
		height: 2px;
		width: 25px;

		background-color: #35323e;
		display: inline-block;

		transition: 250ms;
	}

	${({ menuOpen }) =>
		menuOpen &&
		`
      & > span:nth-child(1) {
         transform: rotateZ(40deg) scaleX(0.6) translate(10px, -3px);
      }

      & > span:nth-child(2) {
         width: 20px;
      }

      & > span:nth-child(3) {
         transform: rotateZ(-40deg) scaleX(0.6) translate(10px, 3px);
      }
      `}

	@media (max-width: 768px) {
		display: flex;
	}
`;
