import styled from 'styled-components';
import { buttonStyle } from 'styled-system';
import { color } from 'styled-system';

export const Statistics = styled.section`
	${color}

	min-height: 100vh;

	display: flex;
	align-items: center;
	flex-direction: column;

	@media (max-width: 768px) {
		padding: 0 2rem;
	}
`;

export const ShortenContainer = styled.div`
	position: relative;
	bottom: 6rem;

	height: 12rem;
	width: 80%;

	background: url('/assets/Meteor.svg');
	background-size: 100% 100%;
	background-repeat: no-repeat;

	@media (max-width: 768px) {
		height: 15rem;
		width: 100%;
	}
`;

export const ShortenForm = styled.form`
	height: 100%;
	width: 100%;
	padding: 4rem 6rem;

	display: flex;
	justify-content: center;
	align-items: center;

	@media (max-width: 768px) {
		padding: 1rem 2rem;

		flex-direction: column;
		justify-content: space-around;
	}
`;

export const ShortenInput = styled.input`
	${color}

	margin: 0 0.5rem;
	padding: 2rem;

	height: 4rem;
	width: 100%;

	border: none;
	border-radius: 0.5rem;

	font-size: 1.4rem;
	font-family: 'Poppins';

	&::placeholder {
		color: inherit;
		opacity: 0.5;
	}
`;

export const ShortenButton = styled.button`
	${buttonStyle}
	${color}

	margin-left: 2rem;
	height: 4rem;
	width: 25%;

	border: none;
	border-radius: 0.5rem;

	font-weight: 700;
	font-size: 1.4rem;
	font-family: 'Poppins';

	cursor: pointer;

	@media (max-width: 768px) {
		width: 100%;
		margin-left: 0;
	}
`;

export const HistoryContainer = styled.div`
	position: relative;
	bottom: 5rem;
	width: 100%;

	display: flex;
	flex-direction: column;
	align-items: center;
`;

export const HistoryEntry = styled.div`
	${color}

	margin: 0.5rem 0;
	padding: 2rem;

	min-height: 5rem;
	width: 75%;

	border-radius: 0.5rem;

	display: flex;
	justify-content: space-between;
	align-items: center;

	@media (max-width: 768px) {
		padding: 0rem;
		width: 100%;

		flex-direction: column;
	}
`;

export const HistoryInput = styled.a`
	${color}

	width: 100%;
	font-size: 1.4rem;

	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;

	@media (max-width: 768px) {
		padding: 1rem;

		flex-direction: column;
		border-bottom: 1px solid #eee;
	}
`;

export const HistorySection = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;

	&:nth-child(1) {
		width: 60%;
	}

	&:nth-child(2) {
		width: 40%;
	}

	@media (max-width: 768px) {
		width: 100% !important;

		flex-direction: column;
		align-items: flex-start;

		&:not(:first-child) {
			padding: 1rem;
		}
	}
`;

export const HistoryOutput = styled.a`
	${color}

	font-size: 1.3rem;

	@media (max-width: 768px) {
		padding: 0rem;
	}
`;

export const HistoryButton = styled.button`
	${buttonStyle}

	height: 3rem;
	width: 7.5rem;

	border: none;
	border-radius: 0.5rem;

	color: #fff;
	font-weight: 700;
	font-size: 1.1rem;
	font-family: 'Poppins';

	cursor: pointer;

	@media (max-width: 768px) {
		width: 100%;
		margin-top: 1rem;
	}
`;

export const StatisticsBanner = styled.div`
	width: 100%;

	display: flex;
	align-items: center;
	flex-direction: column;
`;

export const StatisticsHeading = styled.h2`
	${color}

	margin: 0;

	font-size: 3.5rem;
	text-align: center;

	@media (max-width: 768px) {
		font-size: 2.5rem;
	}
`;

export const StatisticsDescription = styled.div`
	${color}
	padding: 1rem 0;
	width: 50%;

	font-size: 1.7rem;
	text-align: center;

	@media (max-width: 768px) {
		padding: 2rem 0;
		width: 100%;
	}
`;
