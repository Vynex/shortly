const colors = {
	darkViolet: '#3b3054',
	veryDarkViolet: '#232127',

	lightGray: '#f0f1f6',
	gray: '#bfbfbf',
	grayishViolet: '#9e9aa7',

	veryDarkBlue: '#35323e',

	cyan: '#2acfcf',
	lightCyan: '#9be3e2',

	red: '#f46262',

	white: '#fff',
};

const theme = {
	...colors,

	buttons: {
		primary: {
			backgroundColor: colors.cyan,
			'&:hover': {
				backgroundColor: colors.lightCyan,
			},
		},
		secondary: {
			backgroundColor: colors.darkViolet,
			'&:hover': {
				backgroundColor: colors.darkViolet,
			},
		},
	},
};

export default theme;
